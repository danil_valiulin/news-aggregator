# Агрегатор новостей

Инициализация бд, билд фронта и приложения
```shell script
make init
```

Запуск сервиса
```shell script
./news-aggregtor start
```
Перейти на `localhost:3000` и пользоваться

Примеры ресурсов можно найти в файле `examples.txt`

