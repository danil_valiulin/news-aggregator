package migrations

import (
	"news-aggregator/services"
)

// Migrate накатываение миграция
func Migrate(args ...string) error {
	return services.NewDatatore().Migrate(args)
}

