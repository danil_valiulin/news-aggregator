package migrations

import (
	"fmt"
	"github.com/go-pg/migrations/v7"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		fmt.Println("creating table resource...")

		_, err := db.Exec(`
			CREATE TABLE IF NOT EXISTS resource (
				id SERIAL PRIMARY KEY,
				url VARCHAR(500) UNIQUE,
				rule JSONB
			);
		`)
		if err != nil {
			return err
		}

		return nil
	}, func(db migrations.DB) error {
		fmt.Println("dropping table resource...")

		_, err := db.Exec(`
			DROP TABLE resource;
		`)
		if err != nil {
			return err
		}

		return nil
	})
}
