package migrations

import (
	"fmt"
	"github.com/go-pg/migrations/v7"
)

func init() {
	migrations.Register(func(db migrations.DB) error {
		fmt.Println("creating table news...")

		_, err := db.Exec(`
			CREATE TABLE IF NOT EXISTS news (
				id SERIAL PRIMARY KEY,
				title TEXT,
				body TEXT,
				created_at TIMESTAMP WITH TIME ZONE DEFAULT now() NOT NULL
			);
		`)
		if err != nil {
			return err
		}

		return nil
	}, func(db migrations.DB) error {
		fmt.Println("dropping table news...")

		_, err := db.Exec(`
			DROP TABLE news;
		`)
		if err != nil {
			return err
		}

		return nil
	})
}
