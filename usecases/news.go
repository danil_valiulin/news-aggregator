package usecases

import (
	"news-aggregator/models"
	"news-aggregator/repository"
)

// NewsUsecase структера usecase'a новостей
type NewsUsecase struct {
	newsRepository INewsRep
}

// NewNewsUsecase конструктор
func NewNewsUsecase(
	newsRep *repository.News,
) *NewsUsecase {
	return &NewsUsecase{
		newsRepository: newsRep,
	}
}

// NewsFindAll поиск всех новостей
func (uc *NewsUsecase) NewsFindAll(title string) ([]models.Article, error) {
	return uc.newsRepository.FindAll(title)
}

// NewsFindByID поиск новости по идентификатору
func (uc *NewsUsecase) NewsFindByID(id int) (*models.Article, error) {
	return uc.newsRepository.FindByID(id)
}
