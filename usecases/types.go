package usecases

import (
	"net/http"
	"news-aggregator/models"
)

// Doer интерфейс http клиента
type Doer interface {
	Do(req *http.Request) (*http.Response, error)
}

// INewsRep интерфейс репозитория новостей
type INewsRep interface {
	FindAll(title string) ([]models.Article, error)
	FindByID(id int) (*models.Article, error)
}

// IResourceRep интерфейс репозитория ресурсов
type IResourceRep interface {
	Save(resource *models.Resource) error
	Delete(id int) error
	FindAll() ([]models.Resource, error)
}

// IQueue интерфес очереди
type IQueue interface {
	Push(resource models.Resource)
}
