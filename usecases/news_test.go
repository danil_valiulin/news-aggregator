package usecases

import (
	"errors"
	"reflect"
	"testing"

	"news-aggregator/models"
	"news-aggregator/usecases/mocks"
)

func TestNewsUsecase_NewsFindAll(t *testing.T) {
	expectedArticle := models.Article{
		ID:    1,
		Title: "success",
		Text:  "great article",
	}

	okRep := &mocks.INewsRep{}
	okRep.On("FindAll", "success").Return(
		[]models.Article{
			expectedArticle,
		},
		nil,
	)

	failRep := &mocks.INewsRep{}
	failRep.On("FindAll", "fail").Return(
		[]models.Article{},
		errors.New("some error"),
	)

	type fields struct {
		newsRepository INewsRep
	}
	type args struct {
		title string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Article
		wantErr bool
	}{
		{
			"Success",
			fields{okRep},
			args{"success"},
			[]models.Article{expectedArticle},
			false,
		},
		{
			"Error",
			fields{failRep},
			args{"fail"},
			[]models.Article{},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &NewsUsecase{
				newsRepository: tt.fields.newsRepository,
			}
			got, err := uc.NewsFindAll(tt.args.title)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewsFindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewsFindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewsUsecase_NewsFindByID(t *testing.T) {
	expectedArticle := &models.Article{
		ID:    1,
		Title: "title",
		Text:  "ok",
	}

	okRep := &mocks.INewsRep{}
	okRep.On("FindByID", 1).Return(
		expectedArticle,
		nil,
	)

	failRep := &mocks.INewsRep{}
	failRep.On("FindByID", 2).Return(
		nil,
		errors.New("some error"),
	)
	type fields struct {
		newsRepository INewsRep
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Article
		wantErr bool
	}{
		{
			"Success",
			fields{okRep},
			args{1},
			expectedArticle,
			false,
		},
		{
			"Error",
			fields{failRep},
			args{2},
			nil,
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &NewsUsecase{
				newsRepository: tt.fields.newsRepository,
			}
			got, err := uc.NewsFindByID(tt.args.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("NewsFindByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewsFindByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}
