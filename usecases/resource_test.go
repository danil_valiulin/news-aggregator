package usecases

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"reflect"
	"testing"

	"news-aggregator/models"
	"news-aggregator/usecases/mocks"

	"github.com/stretchr/testify/mock"
)

func TestResourceUsecase_DeleteByID(t *testing.T) {
	okRep := &mocks.IResourceRep{}
	okRep.On("Delete", 1).Return(nil)

	failRep := &mocks.IResourceRep{}
	failRep.On("Delete", 2).Return(
		errors.New("some error"),
	)

	type fields struct {
		client             Doer
		queue              IQueue
		newsRepository     INewsRep
		resourceRepository IResourceRep
	}
	type args struct {
		id int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"success",
			fields{
				client:             nil,
				queue:              nil,
				newsRepository:     nil,
				resourceRepository: okRep,
			},
			args{1},
			false,
		},
		{
			"error",
			fields{
				client:             nil,
				queue:              nil,
				newsRepository:     nil,
				resourceRepository: failRep,
			},
			args{2},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &ResourceUsecase{
				client:             tt.fields.client,
				queue:              tt.fields.queue,
				newsRepository:     tt.fields.newsRepository,
				resourceRepository: tt.fields.resourceRepository,
			}
			if err := uc.DeleteByID(tt.args.id); (err != nil) != tt.wantErr {
				t.Errorf("DeleteByID() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestResourceUsecase_FindAll(t *testing.T) {
	expected := []models.Resource{
		{
			ID:      1,
			Address: "addr",
			Rule: models.Rules{
				HrefSelector: "",
			},
		},
	}

	okRep := &mocks.IResourceRep{}
	okRep.On("FindAll").Return(
		expected,
		nil,
	)

	failRep := &mocks.IResourceRep{}
	failRep.On("FindAll").Return(
		[]models.Resource{},
		errors.New("some error"),
	)

	type fields struct {
		client             Doer
		queue              IQueue
		newsRepository     INewsRep
		resourceRepository IResourceRep
	}
	tests := []struct {
		name    string
		fields  fields
		want    []models.Resource
		wantErr bool
	}{
		{
			"success",
			fields{
				client:             nil,
				queue:              nil,
				newsRepository:     nil,
				resourceRepository: okRep,
			},
			expected,
			false,
		},
		{
			"errors",
			fields{
				client:             nil,
				queue:              nil,
				newsRepository:     nil,
				resourceRepository: failRep,
			},
			[]models.Resource{},
			true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &ResourceUsecase{
				client:             tt.fields.client,
				queue:              tt.fields.queue,
				newsRepository:     tt.fields.newsRepository,
				resourceRepository: tt.fields.resourceRepository,
			}
			got, err := uc.FindAll()
			if (err != nil) != tt.wantErr {
				t.Errorf("FindAll() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("FindAll() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestResourceUsecase_ResourceSave(t *testing.T) {
	resource := models.Resource{Address: "localhost"}

	clientOk := &mocks.Doer{}
	clientOk.On("Do", mock.Anything).Return(
		&http.Response{
			Body: ioutil.NopCloser(bytes.NewBufferString("Hello World")),
			Header: map[string][]string{
				"Content-Type": []string{"text/html"},
			},
			StatusCode: 200,
		},
		nil,
	)

	okRep := &mocks.IResourceRep{}
	okRep.On("Save", mock.Anything).Return(nil)

	q := &mocks.IQueue{}
	q.On("Push", mock.Anything)

	type fields struct {
		client             Doer
		queue              IQueue
		newsRepository     INewsRep
		resourceRepository IResourceRep
	}
	type args struct {
		resource models.Resource
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"stay positive",
			fields{
				client:             clientOk,
				queue:              q,
				newsRepository:     nil,
				resourceRepository: okRep,
			},
			args{resource},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &ResourceUsecase{
				client:             tt.fields.client,
				queue:              tt.fields.queue,
				newsRepository:     tt.fields.newsRepository,
				resourceRepository: tt.fields.resourceRepository,
			}
			if err := uc.ResourceSave(tt.args.resource); (err != nil) != tt.wantErr {
				t.Errorf("ResourceSave() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestResourceUsecase_checkResponse(t *testing.T) {
	type fields struct {
		client             Doer
		queue              IQueue
		newsRepository     INewsRep
		resourceRepository IResourceRep
	}
	type args struct {
		resp *http.Response
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &ResourceUsecase{
				client:             tt.fields.client,
				queue:              tt.fields.queue,
				newsRepository:     tt.fields.newsRepository,
				resourceRepository: tt.fields.resourceRepository,
			}
			if err := uc.checkResponse(tt.args.resp); (err != nil) != tt.wantErr {
				t.Errorf("checkResponse() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestResourceUsecase_getResponse(t *testing.T) {
	type fields struct {
		client             Doer
		queue              IQueue
		newsRepository     INewsRep
		resourceRepository IResourceRep
	}
	type args struct {
		url string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *http.Response
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &ResourceUsecase{
				client:             tt.fields.client,
				queue:              tt.fields.queue,
				newsRepository:     tt.fields.newsRepository,
				resourceRepository: tt.fields.resourceRepository,
			}
			got, err := uc.getResponse(tt.args.url)
			if (err != nil) != tt.wantErr {
				t.Errorf("getResponse() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("getResponse() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestResourceUsecase_initQueue(t *testing.T) {
	type fields struct {
		client             Doer
		queue              IQueue
		newsRepository     INewsRep
		resourceRepository IResourceRep
	}
	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			uc := &ResourceUsecase{
				client:             tt.fields.client,
				queue:              tt.fields.queue,
				newsRepository:     tt.fields.newsRepository,
				resourceRepository: tt.fields.resourceRepository,
			}
			if err := uc.initQueue(); (err != nil) != tt.wantErr {
				t.Errorf("initQueue() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
