package usecases

import (
	"fmt"
	"net/http"
	"strings"

	"news-aggregator/models"
	"news-aggregator/services"
)

const (
	html = "text/html"
	rss  = "application/rss+xml"
)

// ResourceUsecase структура usecase'a ресурсов
type ResourceUsecase struct {
	client             Doer
	queue              IQueue
	newsRepository     INewsRep
	resourceRepository IResourceRep
}

// NewResourceUsecase конструктор
func NewResourceUsecase(
	queue *services.Queue,
	newsRep INewsRep,
	resourceRep IResourceRep,
	client Doer,
) (*ResourceUsecase, error) {
	uc := &ResourceUsecase{
		queue:              queue,
		newsRepository:     newsRep,
		resourceRepository: resourceRep,
		client:             client,
	}

	if err := uc.initQueue(); err != nil {
		return nil, err
	}

	return uc, nil
}

func (uc *ResourceUsecase) initQueue() error {
	resources, err := uc.FindAll()
	if err != nil {
		return err
	}

	for _, r := range resources {
		uc.queue.Push(r)
	}

	fmt.Println("queue was init")

	return nil
}

// FindAll поиск всех ресурсов
func (uc *ResourceUsecase) FindAll() ([]models.Resource, error) {
	return uc.resourceRepository.FindAll()
}

// DeleteByID удаление ресурса по идентификатору
func (uc *ResourceUsecase) DeleteByID(id int) error {
	// TODO как удалить из очереди?
	return uc.resourceRepository.Delete(id)
}

// ResourceSave проверит отвечает ли русерс с нужными заголовками и
// сохранит его в базу в случае успеха
func (uc *ResourceUsecase) ResourceSave(resource models.Resource) error {
	resourceResponse, err := uc.getResponse(resource.Address)
	if err != nil {
		return err
	}

	if err := uc.checkResponse(resourceResponse); err != nil {
		return err
	}

	if err := uc.resourceRepository.Save(&resource); err != nil {
		return err
	}

	uc.queue.Push(resource)

	return nil
}

func (uc *ResourceUsecase) checkResponse(resp *http.Response) error {
	fmt.Println("status code", resp.StatusCode)
	if resp.StatusCode != 200 {
		return fmt.Errorf("resource respond %d status code", resp.StatusCode)
	}

	contentType := resp.Header.Get("Content-Type")
	if !strings.Contains(contentType, html) && !strings.Contains(contentType, rss) {
		return fmt.Errorf("unsupported content type")
	}

	return nil
}

func (uc *ResourceUsecase) getResponse(url string) (*http.Response, error) {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	response, err := uc.client.Do(request)
	if err != nil {
		return nil, err
	}

	return response, nil
}
