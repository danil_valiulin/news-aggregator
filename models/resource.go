package models

// Resource модель ресурса
type Resource struct {
	TableName struct{} `json:"-" sql:"resource"`

	ID      int    `json:"id" sql:"id"`
	Address string `json:"address" sql:"url"`
	Rule    Rules  `json:"rule" sql:"rule,json"`
}

// Rules структура правил
type Rules struct {
	HrefSelector     string `json:"href_selector"`
	TitleSelector    string `json:"article_title_selector" validate:"required"`
	TextSelector     string `json:"article_text_selector" validate:"required"`
	InnerTagSelector string `json:"inner_tag_selector,omitempty"`
}
