package models

// Article модель статьи
type Article struct {
	TableName struct{} `sql:"news" json:"-"`

	ID    int    `json:"id" sql:"id"`
	Title string `json:"title" sql:"title"`
	Text  string `json:"body,omitempty" sql:"body"`
}
