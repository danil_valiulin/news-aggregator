package forms

import "news-aggregator/models"

// ResourceForm форма создания ресурса
type ResourceForm struct {
	Addr  string `json:"address" validate:"required,url"`
	Rules Rules  `json:"rules" validate:"dive"`
}

// Rules правила парсинга
type Rules struct {
	HrefSelector     string `json:"href_selector"`
	TitleSelector    string `json:"article_title_selector" validate:"required"`
	TextSelector     string `json:"article_text_selector" validate:"required"`
	InnerTagSelector string `json:"inner_tag_selector,omitempty"`
}

func (rf *ResourceForm) Model() models.Resource {
	return models.Resource{
		Address: rf.Addr,
		Rule: models.Rules{
			HrefSelector:     rf.Rules.HrefSelector,
			TitleSelector:    rf.Rules.TitleSelector,
			TextSelector:     rf.Rules.TextSelector,
			InnerTagSelector: rf.Rules.InnerTagSelector,
		},
	}
}
