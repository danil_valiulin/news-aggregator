package http

import (
	"net/http"
	"strconv"

	"news-aggregator/http/forms"

	"github.com/labstack/echo"
)

// List список ресурсов
func (h *BaseHandler) List(ctx echo.Context) error {
	response, err := h.resourceUsecase.FindAll()
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	return ctx.JSON(http.StatusOK, response)
}

// AddResource добавление ресурса
func (h *BaseHandler) AddResource(ctx echo.Context) error {
	form := &forms.ResourceForm{}

	if err := ctx.Bind(form); err != nil {
		return ctx.JSON(http.StatusBadRequest, err.Error())
	}

	if err := h.vaidator.Struct(form); err != nil {
		return ctx.JSON(http.StatusUnprocessableEntity, err.Error())
	}

	resource := form.Model()

	if err := h.resourceUsecase.ResourceSave(resource); err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	return ctx.NoContent(http.StatusOK)
}

// DeleteResource удаление ресурса
func (h *BaseHandler) DeleteResource(ctx echo.Context) error {
	idParam := ctx.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		return ctx.JSON(http.StatusUnprocessableEntity, "id must be number")
	}

	if err := h.resourceUsecase.DeleteByID(id); err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	return ctx.NoContent(http.StatusNoContent)
}
