package http

import (
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

// NewsList список новостей
func (h *BaseHandler) NewsList(ctx echo.Context) error {
	title := ctx.QueryParam("title")

	response, err := h.newsUsecase.NewsFindAll(title)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	return ctx.JSON(http.StatusOK, response)
}

// NewsByID поиск новости по идентификатору
func (h *BaseHandler) NewsByID(ctx echo.Context) error {
	idParam := ctx.Param("id")
	id, err := strconv.Atoi(idParam)
	if err != nil {
		return ctx.JSON(http.StatusUnprocessableEntity, "id must be number")
	}

	response, err := h.newsUsecase.NewsFindByID(id)
	if err != nil {
		return ctx.JSON(http.StatusInternalServerError, err.Error())
	}

	if response == nil {
		return ctx.NoContent(http.StatusNotFound)
	}

	return ctx.JSON(http.StatusOK, response)
}
