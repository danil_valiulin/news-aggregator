package http

import (
	"gopkg.in/go-playground/validator.v9"
	"news-aggregator/usecases"
)

// BaseHandler структра хэнделеров
type BaseHandler struct {
	vaidator        *validator.Validate
	newsUsecase     *usecases.NewsUsecase
	resourceUsecase *usecases.ResourceUsecase
}

// NewHandlers конструктор
func NewHandlers(
	v *validator.Validate,
	newsUC *usecases.NewsUsecase,
	resourceUC *usecases.ResourceUsecase,
) *BaseHandler {
	return &BaseHandler{
		vaidator:        v,
		newsUsecase:     newsUC,
		resourceUsecase: resourceUC,
	}
}
