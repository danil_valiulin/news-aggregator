import React, { Component } from 'react';
import './App.css';
import Tabs from './components/Tabs';
import { BrowserRouter as Router, Route } from "react-router-dom";
import Article from "./components/Article";

class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <div className="header"/>
          <Route path="/" exact component={Tabs} />
          <Route path="/article/:id" exact component={Article} />
        </div>
      </Router>
  );
  }
}

export default App;