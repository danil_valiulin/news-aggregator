import axios from 'axios'

const api = axios.create({
  baseURL: 'http://localhost:3000/api'
});

export default {
  fetchNews: () => api.get('/news'),
  fetchArticle: (id) => api.get(`/news/${id}`),
  fetchSources: () => api.get('/resource'),
  createSource: (address, rules) => api.post('/resource', { address, rules }),
  deleteSource: (id) => api.delete(`/resource/${id}`),
  searchNews: (term) => api.get(`/news?title=${term}`),
}
