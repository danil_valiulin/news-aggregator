import * as React from 'react'
import { Link } from 'react-router-dom'
import { Card } from 'antd';

export default class NewsCard extends React.Component {
  render() {
    const { id, title, body } = this.props;

    return (
      <Card
        className='news-list__item'
        type="inner"
        title={title}
        extra={<Link to={`/article/${id}`}>Читать полностью</Link>}
      >
        {body}
      </Card>
    )
  }
}
