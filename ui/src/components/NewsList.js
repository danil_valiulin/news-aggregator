import * as React from 'react';
import { Input } from 'antd';
import api from '../api'
import NewsCard from "./NewsCard";

const { Search } = Input;

let intervalID = null;

export default class NewsList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      news: [],
      fetching: false,
      searchTerm: null
    }
  }

  componentDidMount() {
    this.setState({ fetching: true });
    this.fetchNews();
    this.setState({ fetching: false });

    intervalID = setInterval(() => {
      if (!this.state.searchTerm) {
        this.fetchNews();
      }
    }, 1500)
  }

  componentWillUnmount() {
    clearInterval(intervalID)
  }

  fetchNews = () => {
    api.fetchNews().then(res => {
      if (res && res.data) {
        const news = res.data.map(item => {
          return {...item, excerpt: item.body
              ? item.body.slice(0, 300) + '...'
              : 'Новость без текстового содержимого 😅'
          }
        });

        this.setState({ news });
      }
    })
  };

  handleSearch = (term) => {
    this.setState({ fetching: true, searchTerm: term });
    api.searchNews(term).then(res => {
      if (res && res.data) {
        const news = res.data.map(item => {
          return {...item, excerpt: item.body
              ? item.body.slice(0, 300) + '...'
              : 'Новость без текстового содержимого 😅'
          }
        });
        this.setState({ news });
      }
      this.setState({ fetching: false });
    })
  };

  render() {
    const { news } = this.state;

    return <div className='news-list'>
      {news.length
        ? <Search
            className='news-list__search'
            placeholder="Поиск новостей по заголовку"
            onSearch={this.handleSearch}
            enterButton
          />
        : <p>Если источники добавлены, то новости появятся, точно по волшебству!</p>
      }
      {news.length > 0 && <p>Всего на странице: {news.length}</p>}
      {news.map(item => {
        return <NewsCard key={item.id} id={item.id} title={item.title} body={item.excerpt} />
      })}
    </div>
  }

}