import * as React from 'react';
import { Card } from "antd";
import Highlight from 'react-highlight';
import './code-hightlighting.css';


export default class SourceCard extends React.Component {
  render() {
    const { id, title, rules, onDelete } = this.props;

    return (
      <Card
        className='source-list__item'
        title={title}
        bordered={true}
        extra={<a onClick={() => onDelete(id)}>Удалить</a>}
      >
        <Highlight language="JSON">
          {JSON.stringify(rules, null, 2)}
        </Highlight>
      </Card>
    )
  }
}
