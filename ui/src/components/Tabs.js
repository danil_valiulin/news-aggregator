import * as React from 'react'
import {Tabs, Icon} from 'antd';
import NewsList from "./NewsList";
import SourceList from "./SourceList";

const {TabPane} = Tabs;

export default class TabsComponent extends React.Component {
  render() {
    return (
      <Tabs defaultActiveKey="1" size='large' animated={false} style={{textAlign: 'center'}}>
        <TabPane tab={
            <span>
              <Icon type="read"/> Новости
            </span>
          }
          key="1"
        >
          <NewsList />
        </TabPane>
        <TabPane
          tab={
            <span>
              <Icon type="global"/> Источники
            </span>
          }
          key="2"
        >
          <SourceList />
        </TabPane>
      </Tabs>

    )
  }
}
