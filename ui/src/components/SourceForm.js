import * as React from 'react';
import {Form, Input, Modal, notification, Icon} from 'antd';
import api from '../api';

const { TextArea } = Input;

const mockSource = {
  pikabu: {
    address: "https://pikabu.ru/hot/time",
    rules: JSON.stringify({
      href_selector: ".story__title-link",
      article_title_selector: ".story__title-link",
      article_text_selector: ".story-block_type_text",
      inner_tag_selector: ""
    }),
  },
  vc: {
    address: "https://vc.ru/rss/all",
    rules: JSON.stringify({
      href_selector: "",
      article_title_selector: ".entry_header__title",
      article_text_selector: ".b-article",
      inner_tag_selector: "p"
    })
  }
};

class SourceForm extends React.Component {
  state = {
    popupIsVisible: false,
    apiError: null,
    fetching: false,
    data: null,
  };

  componentWillReceiveProps(nextProps, nextContext) {
    if (nextProps.visible) {
      this.showModal();
    }
  }

  showModal = () => {
    this.setState({
      popupIsVisible: true,
      data: null
    });
  };

  handleCancel = () => {
    this.setState({
      popupIsVisible: false,
      data: null
    });
    this.props.form.resetFields()
  };

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        const { address, rules } = values;
        try {
          const ruleObj = JSON.parse(rules);
          this.setState({fetching: true});

          api.createSource(address, ruleObj)
            .then((response) => {
              if (response && response.status === 200) {
                this.props.onAddNewSource();
                this.handleCancel();
              } else {
                console.log('response:',response);
              }
            })
            .catch((error) => {
              this.openNotification(error.message)
            })
            .finally(() => {
              this.setState({fetching: false});
            })
        } catch (error) {
          this.setState({apiError: 'Это не похоже на JSON'})
        }
      }
    });
  };

  openNotification = (message) => {
    console.log('message:',message);
    notification.open({
      message: 'Сервер недоволен',
      description: message,
      icon: <Icon type="frown" style={{ color: '#108ee9' }} />,
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    const { popupIsVisible, apiError } = this.state;

    return (
      <Modal
        title="Добавление источника"
        visible={popupIsVisible}
        onOk={this.handleSubmit}
        onCancel={this.handleCancel}
        okButtonProps={{
          loading: this.state.fetching
        }}
      >
        <Form onSubmit={this.handleSubmit}>
          <div>
            Заполнить тестовыми данными:
            <a onClick={() => this.setState({data: mockSource.pikabu})}>#1</a>,
            <a onClick={() => this.setState({data: mockSource.vc})}>#2</a>
          </div>
          <Form.Item>
            {getFieldDecorator('address', {
              rules: [{ required: true, message: 'У источника должно быть адрес' }],
              initialValue: this.state.data && this.state.data.address
            })(
              <Input
                placeholder="Адрес источника, например: https://www.pikabu.ru/"
              />,
            )}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('rules', {
              rules: [{ required: true, message: 'Необходимо описать правила',  }],
              initialValue: this.state.data && this.state.data.rules
            })(
              <TextArea placeholder="Правила парсинга в формате JSON" rows={4} />,
            )}
          </Form.Item>
          {apiError && <p>{ apiError }</p>}
        </Form>
      </Modal>
    );
  }
}

const WrappedSourceForm = Form.create({ name: 'normal_login' })(SourceForm);
export default WrappedSourceForm;