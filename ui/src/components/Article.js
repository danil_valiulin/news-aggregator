import * as React from 'react';
import { Button, Icon } from 'antd';
import { Link } from 'react-router-dom'
import api from "../api";

export default class Article extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: '',
      body: '',
      fetching: false
    }
  }

  componentDidMount() {
    this.setState({ fetching: true });
    const { id } = this.props.match.params;

    if (id) {
      api.fetchArticle(id).then(res => {
        if (res && res.data) {
          const { title, body } = res.data;
          this.setState({
            title: title || '',
            body: body || ''
          })
        }
        this.setState({ fetching: false });
      })

    }
  }

  render() {
    const { title, body } = this.state;

    return (
      <div className="article-body">
        <Link to='/'><Button><Icon type="left" /> К списку новостей</Button></Link>
        <h2>{title}</h2>
        <p>{body}</p>
      </div>
    )
  }
}