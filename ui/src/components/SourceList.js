import * as React from 'react';
import {Icon, Button, Row, Typography} from 'antd';
import api from "../api";
import SourceCard from "./SourceCard";
import SourceForm from './SourceForm'

const { Title } = Typography;

export default class SourceList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      sources: [],
      formIsVisible: false,
      fetching: false
    }
  }

  componentDidMount() {
    this.fetchSources();
  }

  fetchSources = () => {
    this.setState({ fetching: true });

    api.fetchSources().then(res => {
      if (res && res.data) {
        this.setState({
          sources: res.data
        })
      }
      this.setState({ fetching: false });
    })
  };

  onAddNewSource = () => {
    this.setState({formIsVisible: false});
    this.fetchSources();
  };

  onDelete = (id) => {
    this.setState({formIsVisible: false});
    api.deleteSource(id).then(() => {
      this.fetchSources();
    });
  };


render() {
    const { sources, formIsVisible } = this.state;

    return (
      <React.Fragment>
        <SourceForm onAddNewSource={this.onAddNewSource} visible={formIsVisible} />
        <div className="source-list">
          <Row gutter={16}>
            <div className="source-control">
              <Button type="primary" onClick={() => this.setState({formIsVisible: true})}><Icon type="plus" /> Добавить источник</Button>
            </div>
            {sources.length === 0
              ? <p>Ух... Тебе выпала честь добавить первый источник! 🤤</p>
              : <Title level={2}>Активные источники</Title>
            }
            {sources.map(source =>
                <SourceCard
                  key={source.id}
                  id={source.id}
                  onDelete={this.onDelete}
                  title={source.address}
                  rules={source.rule}
                />
              )}
          </Row>
        </div>
      </React.Fragment>
    )
  }
}