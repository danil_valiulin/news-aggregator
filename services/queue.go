package services

import (
	"errors"
	"news-aggregator/models"
	"sync"
)

var ErrEmptyQueue = errors.New("queue is empty")

// Queue структура очереди
type Queue struct {
	resources []models.Resource
	mu        *sync.Mutex
}

// NewQueue конструктор
func NewQueue() *Queue {
	mu := &sync.Mutex{}
	return &Queue{
		resources: make([]models.Resource, 0),
		mu:        mu,
	}
}

// Push кладем в очередь
func (q *Queue) Push(resource models.Resource) {
	q.mu.Lock()
	defer q.mu.Unlock()

	q.resources = append(q.resources, resource)
}

// Pop берем им очереди
func (q *Queue) Pop() (models.Resource, error) {
	q.mu.Lock()
	defer q.mu.Unlock()

	if len(q.resources) == 0 {
		return models.Resource{}, ErrEmptyQueue
	}

	resource := q.resources[0]
	q.resources = q.resources[1:]

	return resource, nil
}
