package services

import (
	"fmt"

	"github.com/go-pg/migrations/v7"
	"github.com/go-pg/pg/v9"
)

// Datastore структура сервиса подключения к бд
type Datastore struct {
	db *pg.DB
}

// NewDatatore конструктор
func NewDatatore() *Datastore {
	db := pg.Connect(&pg.Options{
		Addr:     "127.0.0.1:5442",
		User:     "news",
		Database: "news",
		PoolSize: 2,
	})

	return &Datastore{db: db}
}

// Migrate накат миграций
func (d *Datastore) Migrate(args []string) error {
	// в случае если маграции накатываются на читаю базу, нужно в начале выполинть init
	oldVersion, newVersion, err := migrations.Run(d.db, args...)
	if err != nil {
		fmt.Printf("migration on db: %s\n", err)
		return err
	}

	if newVersion != oldVersion {
		fmt.Printf("migrated from version %d to %d\n", oldVersion, newVersion)
	} else {
		if oldVersion == 0 {
			fmt.Println("No migrations have been applied")
		} else {
			fmt.Printf("%d migrations already applied\n", oldVersion)
		}
	}

	return nil
}

func (d *Datastore) Conn() *pg.DB {
	return d.db
}
