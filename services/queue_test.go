package services

import (
	"github.com/stretchr/testify/require"
	"news-aggregator/models"
	"testing"
)

func TestQueue(t *testing.T) {
	resource := models.Resource{
		ID:        1,
		Address:   "localhost",
		Rule:      models.Rules{},
	}

	q := NewQueue()
	q.Push(resource)

	res, err := q.Pop()
	require.NoError(t, err)
	require.Equal(t, resource, res)

	res, err = q.Pop()
	require.Error(t, err)
	require.Equal(t, models.Resource{}, res)
}
