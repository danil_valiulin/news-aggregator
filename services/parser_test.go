package services

import (
	"bytes"
	"github.com/mmcdole/gofeed"
	"github.com/stretchr/testify/mock"
	"io/ioutil"
	"net/http"
	"news-aggregator/models"
	"news-aggregator/services/mocks"
	"testing"
	"time"
)

func TestParser_parsing(t *testing.T) {
	htmlWithTitle := `<a class="href" href="http://testl.ru"></a>`
	htmlWithArticle := `
	<p class="title">this is title</p>
	<div class="text">
		<p>text1</p>
		<p>text2</p>
	</div>
`

	resRep := &mocks.IResourceRep{}
	resRep.On("Exists", mock.Anything).Return(true, nil)

	client := &mocks.Doer{}
	client.On("Do", mock.Anything).Return(
		&http.Response{
			Body: ioutil.NopCloser(bytes.NewBufferString(htmlWithTitle)),
			Header: map[string][]string{
				"Content-Type": []string{"text/html"},
			},
			StatusCode: 200,
		},
		nil,
	).Once()
	client.On("Do", mock.Anything).Return(
		&http.Response{
			Body: ioutil.NopCloser(bytes.NewBufferString(htmlWithArticle)),
			Header: map[string][]string{
				"Content-Type": []string{"text/html"},
			},
			StatusCode: 200,
		},
		nil,
	).Once()

	expected := models.Article{
		ID:        0,
		Title:     "this is title",
		Text:      "text1text2",
	}

	newsRep := &mocks.INewsRep{}
	newsRep.On("Save", expected).Return(nil)

	resource := models.Resource{
		ID:      1,
		Address: "http://test.ru",
		Rule: models.Rules{
			HrefSelector:     ".href",
			TitleSelector:    ".title",
			TextSelector:     ".text",
			InnerTagSelector: "p",
		},
	}

	type fields struct {
		client  Doer
		fp      *gofeed.Parser
		timeout time.Duration
		queue   IQueue
		newsRep INewsRep
		resRep  IResourceRep
	}
	type args struct {
		resource models.Resource
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			"stay positive",
			fields{
				client:  client,
				fp:      gofeed.NewParser(),
				timeout: 0,
				queue:   nil,
				newsRep: newsRep,
				resRep:  resRep,
			},
			args{resource},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := &Parser{
				client:  tt.fields.client,
				fp:      tt.fields.fp,
				timeout: tt.fields.timeout,
				queue:   tt.fields.queue,
				newsRep: tt.fields.newsRep,
				resRep:  tt.fields.resRep,
			}
			if err := p.parsing(tt.args.resource); (err != nil) != tt.wantErr {
				t.Errorf("parsing() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
