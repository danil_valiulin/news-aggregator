package services

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"

	"github.com/PuerkitoBio/goquery"
	"github.com/mmcdole/gofeed"
	"golang.org/x/net/html/charset"
	"news-aggregator/models"
)

var errResourceNotExists = errors.New("resource not exists")

const (
	html = "text/html"
	rss  = "application/rss+xml"
)

// Doer интерфейс для http клинта
type Doer interface {
	Do(req *http.Request) (*http.Response, error)
}

type IQueue interface {
	Push(resource models.Resource)
	Pop() (models.Resource, error)
}

type INewsRep interface {
	Save(article models.Article) error
}

type IResourceRep interface {
	Exists(url string) (bool, error)
}

// Parser струеткра сервиса парсинга
type Parser struct {
	client  Doer
	fp      *gofeed.Parser
	timeout time.Duration
	queue   IQueue
	newsRep INewsRep
	resRep  IResourceRep
}

// NewParser конструктор
func NewParser(
	client Doer,
	queue IQueue,
	timeout int,
	newsRep INewsRep,
	resRep IResourceRep,
) *Parser {
	fp := gofeed.NewParser()
	service := &Parser{
		client:  client,
		fp:      fp,
		timeout: time.Duration(timeout) * time.Second,
		newsRep: newsRep,
		queue:   queue,
		resRep:  resRep,
	}

	return service
}

// Start ф-ция запускается в фоне, берет ресурсы из очереди и начинает опрашивать
func (p *Parser) Start() {
	for {
		resource, err := p.queue.Pop()
		if err != nil {
			fmt.Println("queue empty")
			time.Sleep(1 * time.Minute)
			continue
		}

		err = p.parsing(resource)
		if err != nil {
			if err == errResourceNotExists {
				continue
			}

			fmt.Printf("parsing error: %v", err)
		}

		p.queue.Push(resource)
		time.Sleep(1 * time.Minute)
	}
}

func (p *Parser) parsing(resource models.Resource) error {
	exists, err := p.resRep.Exists(resource.Address)
	if err != nil {
		return fmt.Errorf("check resource error: %v", err)
	}

	if !exists {
		return errResourceNotExists
	}

	fmt.Println("start parse", resource.Address)

	resp, err := p.getResponse(resource.Address)
	if err != nil {
		return fmt.Errorf("bad response error: %v", err)
	}

	if err := p.parseURL(resp, resource); err != nil {
		return fmt.Errorf("parse error: %v", err)
	}

	return nil
}

func (p *Parser) parseURL(resp *http.Response, resource models.Resource) error {
	links, err := p.getLinks(resp, resource)
	if err != nil {
		return err
	}

	for _, link := range links {
		article, err := p.parseArticle(link, resource.Rule)
		if err != nil {
			return err
		}

		if err := p.newsRep.Save(article); err != nil {
			return err
		}

		time.Sleep(500 * time.Millisecond)
	}

	return nil
}

func (p *Parser) getLinks(resp *http.Response, resource models.Resource) ([]string, error) {
	// TODO  парсить заголовок
	if strings.Contains(resp.Header.Get("Content-Type"), html) {
		return p.parserHTMLLinks(resp.Body, resource)
	}

	if strings.Contains(resp.Header.Get("Content-Type"), rss) {
		return p.parseRSSLinks(resp.Body)
	}

	return nil, fmt.Errorf("not supported MEM type")
}

func (p *Parser) parserHTMLLinks(body io.Reader, resource models.Resource) ([]string, error) {
	var links []string

	if resource.Rule.TitleSelector == "" {
		return nil, fmt.Errorf("title selector can't be emty when parser html")
	}

	doc, err := goquery.NewDocumentFromReader(body)
	if err != nil {
		return nil, err
	}

	title := doc.Find(resource.Rule.HrefSelector)
	title.Each(func(i int, selection *goquery.Selection) {
		href, ok := selection.Attr("href")
		if !ok {
			return
		}

		if !strings.Contains(href, "http") {
			href = resource.Address + href
		}
		links = append(links, href)
	})

	if len(links) == 0 {
		return nil, fmt.Errorf("nothing found")
	}

	return links, nil
}

func (p *Parser) parseRSSLinks(body io.Reader) ([]string, error) {
	links := []string{}
	feed, err := p.fp.Parse(body)
	if err != nil {
		return nil, err
	}

	for _, item := range feed.Items {
		links = append(links, item.Link)
	}

	return links, nil
}

func (p *Parser) parseArticle(addr string, rules models.Rules) (models.Article, error) {
	article := models.Article{}

	resp, err := p.getResponse(addr)
	if err != nil {
		return article, err
	}

	defer resp.Body.Close()

	utf8, err := charset.NewReader(resp.Body, resp.Header.Get("Content-Type"))
	if err != nil {
		return article, err
	}

	doc, err := goquery.NewDocumentFromReader(utf8)
	if err != nil {
		return article, err
	}

	article.Title = strings.TrimSpace(doc.Find(rules.TitleSelector).Text())
	articleDoc := doc.Find(rules.TextSelector)
	if rules.InnerTagSelector != "" {
		article.Text = articleDoc.Find(rules.InnerTagSelector).Text()
	} else {
		article.Text = articleDoc.Text()
	}

	article.Text = strings.TrimSpace(article.Text)

	return article, nil
}

func (p *Parser) getResponse(url string) (*http.Response, error) {
	request, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	response, err := p.client.Do(request)
	if err != nil {
		return nil, err
	}

	return response, nil
}
