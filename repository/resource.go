package repository

import (
	"github.com/go-pg/pg/v9"
	"news-aggregator/models"
)

// Resource структера репозитория ресурсов
type Resource struct {
	db *pg.DB
}

// NewResourceRepository конструктор
func NewResourceRepository(db *pg.DB) *Resource {
	return &Resource{
		db: db,
	}
}

// Save сохранение ресурса в бд
func (r *Resource) Save(resource *models.Resource) error {
	_, err := r.db.Model(resource).Insert()

	return err
}

// Delete удаление ресурса из бд по идентификатору
func (r *Resource) Delete(id int) error {
	m := &models.Resource{}

	_, err := r.db.Model(m).Where("id = ?", id).Delete()

	return err
}

// FindAll поиск всех ресурсов в бд
func (r *Resource) FindAll() ([]models.Resource, error) {
	m := []models.Resource{}

	err := r.db.Model(&m).Select()

	return m, err
}

// Exists проверка есть ли ресурс с такоим url в базе
func (r *Resource) Exists(url string) (bool, error) {
	m := &models.Resource{}

	return r.db.Model(m).Where("url = ?", url).Exists()
}
