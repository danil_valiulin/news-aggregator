package repository

import (
	"fmt"
	"github.com/go-pg/pg/v9"
	"news-aggregator/models"
)

// News структура репозитория новостей
type News struct {
	db *pg.DB
}

// NewNewsRepository констурктор
func NewNewsRepository(db *pg.DB) *News {
	return &News{
		db: db,
	}
}

// Save сохранение новости/статьи
func (n *News) Save(article models.Article) error {
	exists, err := n.db.Model(&article).Where("title = ?", article.Title).Exists()
	if err != nil {
		return err
	}

	if !exists {
		fmt.Println("added new news")
		_, err := n.db.Model(&article).Insert()
		if err != nil {
			return err
		}
	}

	return nil
}

// FindAll поиск всех новостей
func (n *News) FindAll(title string) ([]models.Article, error) {
	m := []models.Article{}
	q := n.db.Model(&m)

	if title != "" {
		q.Where(`title ILIKE ?`, "%"+title+"%")
	}

	q.Order("created_at DESC")
	err := q.Select()

	return m, err
}

// FindByID поиск по идентификатору
func (n *News) FindByID(id int) (*models.Article, error) {
	m := &models.Article{}

	err := n.db.Model(m).
		Where("id = ?", id).
		Select()

	if err != nil {
		if err == pg.ErrNoRows {
			return nil, nil
		}

		return nil, err
	}

	return m, err
}
