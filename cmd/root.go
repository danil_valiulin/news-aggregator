package cmd

import (
	"fmt"
	"github.com/spf13/cobra"
)

func Root() *cobra.Command {
	root := &cobra.Command{
		Use:   "news-aggregator",
		Short: "your news aggregation",
		Run: func(cmd *cobra.Command, args []string) {
			fmt.Println(`"Type "news-aggregator help" for detail usage`)
		},
	}

	root.AddCommand(
		startCommand(),
		migrateCommand(),
	)

	return root
}
