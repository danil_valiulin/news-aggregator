package cmd

import (
	"github.com/spf13/cobra"
	"news-aggregator/app"
)

func startCommand() *cobra.Command {
	return &cobra.Command{
		Use: "start",
		Short: "Start service",
		Run: func(cmd *cobra.Command, args []string) {
			app.Start()
		},
	}
}