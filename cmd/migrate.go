package cmd

import (
	"github.com/spf13/cobra"
	"news-aggregator/migrations"
)

func migrateCommand() *cobra.Command {
	return &cobra.Command{
		Use:   "migrate",
		Short: "Start migrations",
		Long:  "Migrates database. Available arguments are up, down, version, init",
		Run: func(cmd *cobra.Command, args []string) {
			if err := migrations.Migrate(args...); err != nil {
				panic(err)
			}
		},
	}
}