package main

import "news-aggregator/cmd"

func main() {
	if err := cmd.Root().Execute(); err != nil {
		panic(err)
	}
}
