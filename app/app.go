package app

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gopkg.in/go-playground/validator.v9"
	http2 "net/http"
	"news-aggregator/http"
	"news-aggregator/repository"
	"news-aggregator/services"
	"news-aggregator/usecases"
)

// Start старт приложения
func Start() {
	server := echo.New()
	client := &http2.Client{}

	v := validator.New()

	datastore := services.NewDatatore()
	queue := services.NewQueue()

	newsRepository := repository.NewNewsRepository(datastore.Conn())
	resourceRepository := repository.NewResourceRepository(datastore.Conn())

	newsUsecase := usecases.NewNewsUsecase(newsRepository)
	resourceUsecase, err := usecases.NewResourceUsecase(
		queue,
		newsRepository,
		resourceRepository,
		client,
	)
	if err != nil {
		panic(err)
	}

	handlers := http.NewHandlers(v, newsUsecase, resourceUsecase)

	parser := services.NewParser(client, queue, 20, newsRepository, resourceRepository)
	go parser.Start()

	server.Pre(middleware.RemoveTrailingSlash())
	server.Use(middleware.CORS())

	server.File("/", "ui/build/index.html")
	server.Static("/static", "ui/build/static")

	api := server.Group("api")
	api.GET("/news", handlers.NewsList)
	api.GET("/news/:id", handlers.NewsByID)

	api.GET("/resource", handlers.List)
	api.POST("/resource", handlers.AddResource)
	api.DELETE("/resource/:id", handlers.DeleteResource)

	server.Logger.Fatal(server.Start(":3000"))
}
