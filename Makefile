start:
	docker-compose up -d

stop:
	docker-compose down

build:
	go build -ldflags "${LDFLAGS}" -v -i -o news-aggregator .

init:
	make start && \
	make build && \
	./news-aggregator migrate init
	./news-aggregator migrate

test:
	go test ./... -cover

mocks:
	mockery -dir usecases -all -output usecases/mocks
	mockery -dir services -all -output services/mocks
