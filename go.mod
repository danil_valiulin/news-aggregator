module news-aggregator

go 1.12

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-pg/migrations/v7 v7.1.1
	github.com/go-pg/pg/v9 v9.0.0-beta.3
	github.com/go-playground/locales v0.12.1 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.2.9 // indirect
	github.com/leodido/go-urn v1.1.0 // indirect
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/spf13/cobra v0.0.5
	github.com/stretchr/testify v1.3.0
	golang.org/x/net v0.0.0-20190420063019-afa5a82059c6
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.29.1
)
